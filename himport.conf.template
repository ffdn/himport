# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

#
# OUTPUT_DIR : The base directory where to put the ledger files
#
OUTPUT_DIR = "./"

#
# OUTPUT_FILES : The file paths template for each journal.
#                   - bank : the bank entries
#                   - sells : the sell entries
#                   - suppliers : the supplier entries
#                   - social : the social entries (tax, etc.)
#                   - pc : chart of accounts
OUTPUT_FILES = {
    "bank":      "exercices/%year%/ecritures.d/banque.journal",
    "sells":     "exercices/%year%/ecritures.d/vente.journal",
    "suppliers": "exercices/%year%/ecritures.d/achat.journal",
    "social":    "exercices/%year%/ecritures.d/cotisations_sociales.journal",
    "chart_of_accounts" : "exercices/%year%/chart_of_accounrs.journal"
}

#
# MYSQL_SETTINGS : Database definition
#
MYSQL_SETTINGS = {
    "host":      "127.0.0.1",
    "database":  "dolibarr",
    "user":      "dolibarr_ro",
    "password":  "",
    "port":      13306,
}

#
# ACCOUNTING_YEARS : Describe the accounting years if not standard (from 1st january to 31 december).
#                    For exemple the first accounting year could start before the 1st january
#
ACCOUNTING_YEARS = {
    ("2012", "2011/01/01", "2012/12/31")
}

#
# TVA_TYPE : Sort of value-added tax. Possible values :
              - 'none' : the value added tax is not handled.
#             - 'standard' : the value added tax is on delivery for goods (billing date), on payment for services (payment date).
#             - 'service_sur_debit' : the value added tax is on delivery for goods (billing date), and on billing date for services.
TVA_TYPE = "standard"


#
# PC_REFS: define the account references. This is a dictionnary of account.
#
PC_REFS = {
    "tva_a_decaisser":      "44551",
    "tva_deductible":       "44562",
    "tva_deductible_1960":  "445621",
    "tva_deductible_2000":  "445622",
    "tva_deductible_immo":  "44566",
    "tva_deductible_immo_1960":  "445661",
    "tva_deductible_immo_2000":  "445662",
    "tva_collecte":         "44571",
    "tva_collecte_1960":    "445711",
    "tva_collecte_2000":    "445712",
    "tva_regul":            "4458",
    "tva_regul_1960":       "44581",
    "tva_regul_2000":       "44582",
    "default_supplier":     "4011999",
    "default_client":       "4111999",
    "default_tier":         "4999999",
    "default_bank":         "512999",
    "default_income":       "7069999",
    "default_expense":      "6199999",
    "fn_custom_codes":      [
        lambda e: u"261" if e.label == u"Souscription part sociale" and e.datev == datetime.date(year=2011, month=9, day=27) else None,
        lambda e: u"1681" if e.bankclass.categ.id == 7 else None,
    ],
}

SOCIAL_REFS = {
    "TAXCFE":           "447",
    "TAXPENALITE":      "447",
}

#
# Plan comptables (nom, description)
#
PC_NAMES = {
    "1":          "capitaux",
    "11":         "capitaux:report à nouveau",
    "110":        "capitaux:report à nouveau:créditeur",
    "119":        "capitaux:report à nouveau:débiteur",
    "12":         "capitaux:résultat",
    "120":        "capitaux:résultat:positif",
    "129":        "capitaux:résultat:négatif",
    "16":         "capitaux:emprunts et dettes",
    "168":        "capitaux:emprunts et dettes:autres",
    "1681":       "capitaux:emprunts et dettes:autres:emprunts",
    "2":          "immobilisations",
    "201":        "immobilisations:incorporelles:frais d établissement",
    "21":         "immobilisations:corporelles",
    "2183":       "immobilisations:corporelles:matériel informatique",
    "2184":       "immobilisations:corporelles:mobilier",
    "26":         "immobilisations:participations",
    "261":        "immobilisations:participations:titres",
    "280":        "immobilisations:amortissements:incorporelles",
    "281":        "immobilisations:amortissements:corporelles",
    "2813":       "immobilisations:amortissements:corporelles:matériel informatique",
    "2814":       "immobilisations:amortissements:corporelles:mobilier",
    "4":          "tiers",
    "40":         "tiers:fournisseurs",
    "4011999":    "tiers:fournisseurs:XXXXXX",
    "41":         "tiers:clients",
    "4111999":    "tiers:clients:XXXXXX",
    "42":         "tiers:comptes rattachés",
    "425":        "tiers:comptes rattachés:avances et accomptes",
    "445":        "tiers:etat:tva",
    "44551":      "tiers:etat:tva:à décaisser",
    "4456":       "tiers:etat:tva:déductible",
    "44562":      "tiers:etat:tva:déductible:immobilisations",
    "445621":     "tiers:etat:tva:déductible:immobilisations:1960",
    "445622":     "tiers:etat:tva:déductible:immobilisations:2000",
    "44566":      "tiers:etat:tva:déductible:autres",
    "445661":     "tiers:etat:tva:déductible:autres:1960",
    "445662":     "tiers:etat:tva:déductible:autres:2000",
    "44567":      "tiers:etat:tva:déductible:crédit à reporter",
    "44571":      "tiers:etat:tva:collecté",
    "445711":     "tiers:etat:tva:collecté:1960",
    "445712":     "tiers:etat:tva:collecté:2000",
    "4458":       "tiers:etat:tva:à régulariser",
    "44581":      "tiers:etat:tva:à régulariser:1960",
    "44582":      "tiers:etat:tva:à régulariser:2000",
    "447":        "tiers:etat:autres impôts",
    "467":        "tiers:autres",
    "468":        "tiers:divers",
    "4686":       "tiers:divers:charges à payer",
    "4999999":    "tiers:XXXXXX",
    "5":          "finances",
    "512":        "finances:banque",
    "512999":     "finances:banque:XXXXXX",
    "532":        "finances:chèques à encaisser",
    "6":          "charges",
    "60":         "charges:achats",
    "604":        "charges:achats:prestation de services",
    "606":        "charges:achats:non-stockés",
    "6061":       "charges:achats:non-stockés:fournitures non stockables",
    "6063":       "charges:achats:non-stockés:fournitures d entretien et petits équipements",
    "6064":       "charges:achats:non-stockés:fournitures administratives",
    "6068":       "charges:achats:non-stockés:autres matières et fournitures",
    "61":         "charges:services",
    "613":        "charges:services:locations",
    "616":        "charges:services:assurances",
    "62":         "charges:autres services",
    "6227":       "charges:autres services:frais d actes",
    "621":        "charges:autres services:personnel extérieur à l entreprise",
    "624":        "charges:autres services:transport de biens et transports collectifs du personnel",
    "625":        "charges:autres services:déplacement missions réceptions",
    "626":        "charges:autres services:pce",
    "627":        "charges:autres services:banque",
    "628":        "charges:autres services:divers",
    "6281":       "charges:autres services:divers:cotisations",
    "63":         "charges:impots",
    "6351":       "charges:impots:autres:direct",
    "63511":      "charges:impots:autres:direct:cet",
    "651":        "charges:redevances",
    "671":        "charges:exceptionnelles",
    "6712":       "charges:exceptionnelles:pénalités fiscales",
    "672":        "charges:charges sur exercices antérieur",
    "6811":       "charges:amortissements",
    "68111":      "charges:amortissements:incorporelles",
    "68112":      "charges:amortissements:corporelles",
    "6999999":    "charges:XXXXXX",
    "7":          "produits",
    "706":        "produits:services",
    "756":        "produits:cotisations",
    "758":        "produits:divers",
    "7585":       "produits:divers:dons manuels",
    "77":         "produits:exceptionnels",
    "7999999":    "produits:XXXXXX",


}

PC_DESCRIPTIONS = {
    "1":          "Capitaux",
    "11":         "Report à nouveau",
    "117":        "Report positif",
    "119":        "Report négatif",
    "12":         "Résultat",
    "120":        "Résultat positif",
    "129":        "Résultat négatif",
    "16":         "Emprunts et dettes assimilés",
    "168":        "Autres Emprunts et dettes assimilés",
    "1681":       "Autres Emprunts assimilés",
    "2":          "Immobilisations",
    "201":        "Frais d'établissement",
    "21":         "Immobilisations corporelles",
    "2183":       "Matériel informatique",
    "26":         "Participations",
    "261":        "Titres de participation",
    "280":        "Amortissements incorporelles",
    "281":        "Amortissements corporelles",
    "2813":       "Amortissements du matériel informatique",
    "2814":       "Amortissements du mobiliers",
    "4":          "Tiers",
    "40":         "Fournisseurs",
    "4011999":    "tiers:fournisseurs:XXXXXX",
    "41":         "Clients",
    "4111999":    "tiers:clients:XXXXXX",
    "42":         "Comptes rattachés",
    "425":        "Avances et accomptes",
    "445":        "Taxe sur la Valeur Ajoutée",
    "44551":      "TVA à décaisser",
    "4456":       "TVA déductible",
    "44562":      "TVA déductible sur immobilisations",
    "44566":      "TVA déductible autres",
    "44567":      "TVA déductible à reporter",
    "4457":       "TVA collectée",
    "44571":      "TVA collectée sur CA",
    "447":        "Autres impôts taxes et versements assimilés",
    "467":        "Autres Tiers",
    "468":        "Tiers divers",
    "4686":       "Charges à payer",
    "5":          "Finances",
    "512":        "Banque",
    "512999":     "finances:banque:XXXXXX",
    "532":        "Chèques à encaisser",
    "6":          "Charges",
    "60":         "Achats",
    "604":        "Prestation de services",
    "606":        "Achats non-stockés",
    "6061":       "Fournitures non stockables",
    "6063":       "Fournitures d'entretien et petits équipements",
    "6064":       "Fournitures administratives",
    "6068":       "Autres matières et fournitures",
    "61":         "Services",
    "613":        "Locations",
    "616":        "Assurances",
    "62":         "Autres services",
    "6227":       "Frais d'actes",
    "621":        "Frais de personnel extérieur à l'entreprise",
    "624":        "Frais de transport de biens et transports collectifs du personnel",
    "625":        "Frais de déplacement",
    "626":        "Frais de postes et des communications électroniques",
    "626101":     "Boîte postale",
    "627":        "Services bancaires et assimilés",
    "628":        "Services divers",
    "6281":       "Cotisations",
    "63":         "Impôts, Taxes",
    "6351":       "Impôts directs",
    "63511":      "Contribution Economique Teritorriale",
    "651":        "Redevances",
    "671":        "Charges exceptionnelles",
    "6712":       "Amendes, Pénalités fiscales",
    "672":        "Charges sur exercices antérieur",
    "6811":       "Amortissements",
    "68111":      "Amortissements incorporelles",
    "68112":      "Amortissements corporelles",
    "6999999":    "charges:XXXXXX",
    "7":          "Produits",
    "706":        "Services",
    "756":        "Cotisations",
    "77":         "Produits exceptionnels",
    "7718":       "Dons manuels",
    "7999999":    "produits:XXXXXX",
}
