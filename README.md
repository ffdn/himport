# What is himport

The goal of himport is to import accounting data from Dolibarr into a set of hledger
files. Currently supported features are:

- bank accounts
- supplier bills (buy)
- customer bills (sell)
- social entries (taxes)
- VAT

To assign each entry to a specific account, you should have a proper chart of accounts
in Dolibarr. Then the himport configuration maps account numbers from Dolibarr to
hledger accounts (see below).

# Supported Dolibarr versions

himport should be compatible with all versions of Dolibarr from 4.X to 14.X.
If you encounter issues with any of these versions, this is a bug and should
be reported.

# Usage from repository

**Installation**

    apt install libmariadb-dev python3-venv

    git clone https://code.ffdn.org/ffdn/himport.git
    cd himport

    python3 -m venv venv
    ./venv/bin/pip install -e .

**Utilisation**

    ./venv/bin/himport

# Configuration

**General**

You can copy the configuration file from `himport.conf.template`

    cp himport.conf.template himport.conf

**Local configuration**

    cp himport.conf.local.template himport.conf.local

This file is used to store SQL information to connect to the dolibarr database

# Authors

- Philippe Le Brouster
- WhilelM
- Baptiste Jonglez
- Sebastien Badia
