# Changelog

All notable changes to this project will be documented in this file.

## [0.2] - 2022-07-24

This major release drop compatibility to python 2 and add compatibility to Dolibarr up to version 14

- Drop compatibility for python 2
- Add compatibility for python 3
- Support Dolibarr versions up to 14
- Add log messages when using `-v`
- Check missing account in Chart of accounts

## [0.1] - 2016-11-12

First initial release
