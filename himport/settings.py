# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import os

CONF_FILES = [
    "himport.conf",
    "himport.conf.local",
]

__settings = None


def get(opt):
    ret = __settings.get(opt)
    if ret is None:
        raise "Settings not found"
    return ret


def get_ledger_account(code):
    account_names = __settings.get("PC_NAMES")
    if code in account_names:
        return account_names[code]
    else:
        return code


__settings = {}


for conf_file in CONF_FILES:
    if os.path.isfile(conf_file):
        with open(conf_file) as f:
            code = compile(f.read(), conf_file, "exec")
            exec(code, __settings)

if __settings is None:
    raise Exception("Need a configuration file. One of %s" % ",".join(CONF_FILES))

if "OUTPUT_DIR" not in __settings:
    raise Exception("need OUTPUT_DIR settings")

if "PC_REFS" not in __settings:
    raise Exception("need PC_REFS settings")

if "PC_NAMES" not in __settings:
    raise Exception("need PC_NAMES settings")

if "ACCOUNTING_YEARS" in __settings:
    __settings["ACCOUNTING_YEARS"] = [
        (
            year,
            datetime.datetime.strptime(dbegin, "%Y/%m/%d").date(),
            datetime.datetime.strptime(dend, "%Y/%m/%d").date(),
        )
        for (year, dbegin, dend) in __settings["ACCOUNTING_YEARS"]
    ]

if "TVA_TYPE" not in __settings or __settings["TVA_TYPE"] not in [
    "standard",
    "service_sur_debit",
    "none",
]:
    raise Exception(
        "need TVA_TYPE settings either: standard | service_sur_debit | none"
    )
