import codecs
import os

from himport import settings


class Writer(object):
    output_files = settings.get("OUTPUT_FILES")
    output_dir = settings.get("OUTPUT_DIR")

    @staticmethod
    def write(journal, entries, years):
        filename = Writer.output_files[journal]
        output = os.path.join(Writer.output_dir, filename)
        entries_by_year = entries.get_by_year()

        for year in entries_by_year:
            if year in years:
                output_file = output.replace("%year%", year)
                output_dir = os.path.dirname(output_file)
                if not os.path.exists(output_dir):
                    os.makedirs(os.path.dirname(output_file))
                elif not os.path.isdir(output_dir):
                    print("Error: %s is not a dir\n" % (output_dir))
                    raise os.error()

                f = codecs.open(output_file, "w", "utf-8")
                for entry in entries_by_year[year]:
                    f.write(entry.get_ledger())
                    f.write("\n")
                f.close()

    @staticmethod
    def write_hreport_chart_of_accounts(years):
        pc_names = settings.get("PC_NAMES")
        pc_descriptions = settings.get("PC_DESCRIPTIONS")
        for year in years:
            filename = Writer.output_files["chart_of_accounts"]
            output_file = os.path.join(Writer.output_dir, filename).replace(
                "%year%", year
            )

            f = codecs.open(output_file, "w", "utf-8")
            f.write("%s/01/01 * Chart of account\n" % (year))
            for pc in sorted(pc_names.keys()):
                name = pc_names[pc]
                if pc in pc_descriptions:
                    desc = pc_descriptions[pc]
                else:
                    desc = name
                s = "    %s    0 ;  %s  %s\n" % (name, pc, desc)
                f.write(s)
            f.close()
